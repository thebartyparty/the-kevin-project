The Kevin Project By Alasdair Barton
======

Hello! Welcome to the git repository for my game. It's licensed under the MIT licence (So it's basically open source), meaning you can do whatever you want with this code and not get done for copyright. Just thought it's nice to tell you that it is impossible to pirate the game at this current stage as it's free. (At the moment)

Ideas are greatly appreciated and I'm crap at coming up with them, so feel free to add them in the issues section. Likewise for bugs.

Oh, and I'm using the Godot game engine (https://godotengine.org/) to make it if you want to actually do anything with the source files.

======

When this game is released, it will be availible at itch.io: https://thebartyparty.itch.io/the-kevin-project
