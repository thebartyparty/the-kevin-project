extends Node

var timer = null

func _ready():
	timer = get_node("Timer")
	timer.connect("timeout", self, "_on_timer_timeout")
	timer.set_wait_time(5.0)
	timer.start()

func _on_timer_timeout():
	get_tree().change_scene("res://Assets/Scenes/Level2.tscn")
