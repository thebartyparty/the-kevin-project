extends KinematicBody2D

export var grav = 400
export var speed = 400
export var jspeed = 150
export var jump_power = 150
var grounded = 0
var jumping = 0
var yspeed
var motion

var key_pressed_left = false
var key_pressed_right = false

var player_lives
var player_is_dead = false
var chosen_player
var player_can_move

func _ready():
	yspeed = 1


func _process(delta):
	if player_can_move:
		if key_pressed_left:
			get_node("AnimatedSprite").set_animation(chosen_player + "_running_left")
		elif key_pressed_right:
			get_node("AnimatedSprite").set_animation(chosen_player + "_running_right")
	if not key_pressed_left and not key_pressed_right:
		get_node("AnimatedSprite").set_animation(chosen_player)
	get_node("AnimatedSprite").play()


func _physics_process(delta):
	if grounded < 0:
		grounded = 0
	motion = Vector2(0, 0)
	if player_can_move:
		if Input.is_action_pressed("move_left"):
			motion += Vector2(-1, 0)
		if Input.is_action_pressed("move_right"):
			motion += Vector2(1, 0)
		if jumping == 1:
			if grounded > 0:
				jump(jump_power)
				grounded = 0
			jumping = 0
	motion = motion*delta*speed*50
	move_and_slide(motion)
	move_and_collide(Vector2(0, 1)*yspeed*delta)
	if yspeed < grav:
		yspeed += 5


func jump(jumpvar):
	yspeed= -jumpvar


func _input(event):
	if player_can_move:
		if event.is_action_pressed("jump"):
			jumping = 1
	if event.is_action_pressed("move_left"):
			key_pressed_left = true
	if event.is_action_pressed("move_right"):
		key_pressed_right = true
	if event.is_action_released("move_left"):
		key_pressed_left = false
	if event.is_action_released("move_right"):
		key_pressed_right = false


func _on_Bottom_body_entered(body):
	if body.is_in_group("kill_player"):
		player_is_dead = true
	grounded += 1


func _on_Bottom_body_exited(body):
	grounded -= 1


func _on_Top_body_entered(body):
	yspeed = 1

func _on_Hitbox_body_entered(body):
	if body.is_in_group("kill_player"):
		player_is_dead = true
