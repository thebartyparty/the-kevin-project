extends Node2D

var pause_menu = load("res://Assets/Scenes/PauseMenu.tscn")
var pause_menu_node

var player_starting_pos = Vector2(850, 30)

var player_speech_1
var player_speech_2
var player_speech_3
var player_speech_4
var whithouse_speech_1
var whithouse_speech_2
var scene_timer
var player

var scene_num = 0
var has_shown_whitehouse_speech = false
var has_shown_player_speech_3 = false

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		var dir = Directory.new()
		dir.remove("res://tempinfo.dump")
		get_tree().quit()

func _ready():
	player_speech_1 = get_node("Player/Speech 1")
	player_speech_2 = get_node("Player/Speech 2")
	player_speech_3 = get_node("Player/Speech 3")
	player_speech_4 = get_node("Player/Speech 4")
	whithouse_speech_1 = get_node("Whitehouse/Speech 1")
	whithouse_speech_2 = get_node("Whitehouse/Speech 2")
	scene_timer = get_node("Scene Timer")
	player = get_node("Player")
	
	var savegame = File.new()
	savegame.open("res://savegame.save", File.READ)
	var loaded_data = parse_json(savegame.get_line())
	savegame.close()
	
	player.chosen_player = loaded_data.chosen_player
	
	player.player_can_move = false
	
	get_node("Dramatic Music").play()
	
	scene_timer.set_wait_time(2.0)
	scene_timer.connect("timeout", self, "show_next_scene")

func _input(event):
	if event is InputEventMouseMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	elif event is InputEventJoypadButton or event is InputEventJoypadMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	if event.is_action_pressed("ui_pause"):
		if not $Whitehouse/Camera2D.is_current():
			pause_menu_node = pause_menu.instance()
			add_child(pause_menu_node)
			$"Pause Menu".position = $Player.position - Vector2(512, 300)
			get_tree().paused = true

func _process(delta):
	if player.player_is_dead == true:
		player.position = player_starting_pos
		player.player_is_dead = false
	if not get_node("Dramatic Music").is_playing():
		get_node("Dramatic Music").play()
	
	if has_shown_whitehouse_speech == false:
		scene_timer.start()
		has_shown_whitehouse_speech = true
	if has_shown_player_speech_3 == false:
		if get_node("Player").position.x < -2936:
			player.player_can_move = false
			player_speech_3.show()
			scene_num = 7
			scene_timer.start()
			has_shown_player_speech_3 = true
	if get_node("Player").position.x < -2968:
		if Input.is_action_pressed("use"):
			var save_data = {
				music_pos = get_node("Dramatic Music").get_playback_position(),
				current_level = 2,
				chosen_player = player.chosen_player,
				}
			var file = File.new()
			file.open("res://savegame.save", File.WRITE)
			file.store_line(to_json(save_data))
			file.close()
			get_tree().change_scene("res://Assets/Scenes/Cutscene2.tscn")

func show_next_scene():
	if scene_num == 0:
		get_node("Whitehouse/Camera2D").make_current()
		scene_timer.start()
		scene_num += 1
	elif scene_num == 1:
		whithouse_speech_1.show()
		scene_timer.start()
		scene_num += 1
	elif scene_num == 2:
		whithouse_speech_2.show()
		whithouse_speech_1.hide()
		get_node("Carpet").queue_free()
		scene_timer.start()
		scene_num += 1
	elif scene_num == 3:
		whithouse_speech_2.hide()
		get_node("Player/Camera2D").make_current()
		scene_timer.start()
		scene_num += 1
	elif scene_num == 4:
		player_speech_1.show()
		scene_timer.start()
		scene_num += 1
	elif scene_num == 5:
		player_speech_2.show()
		player_speech_1.hide()
		player.player_can_move = true
		scene_timer.start()
		scene_num += 1
	elif scene_num == 6:
		player_speech_2.hide()
		scene_timer.stop()
	elif scene_num == 7:
		player_speech_4.show()
		player_speech_3.hide()
		player.player_can_move = true
		scene_num += 1
	elif scene_num == 8:
		player_speech_4.hide()
		scene_timer.stop()