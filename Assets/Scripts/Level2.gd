extends Node2D

var pause_menu = load("res://Assets/Scenes/PauseMenu.tscn")
var pause_menu_node

var player_starting_pos = Vector2(52, 227)

var player

var scene_timer
var fire_rate_timer
var toggle_shooting_timer
var terminal_1_timer
var terminal_2_timer
var terminal_3_timer

var shown_player_speech_2 = false
var current_bullet_num
var player_touching_door = false
var mead_can_shoot = false
var player_touching_terminal_1 = false
var terminal_1_activated = false
var player_touching_terminal_2 = false
var terminal_2_activated = false
var player_touching_terminal_3 = false
var terminal_3_activated = false

var scene_num = 0

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		var dir = Directory.new()
		dir.remove("res://tempinfo.dump")
		get_tree().quit()

func _ready():
	player = get_node("Player")
	
	var loaded_data = {}
	var file = File.new()
	file.open("res://savegame.save", File.READ)
	loaded_data = parse_json(file.get_line())
	get_node("Dramatic Music").play(loaded_data.music_pos)
	player.chosen_player = loaded_data.chosen_player
	
	current_bullet_num = 1
	
	get_node("Level Section 1").show()
	get_node("Section 2 Barrier").show()
	get_node("Backgrounds/Background 2 1").show()
	get_node("Terminal 1").show()
	get_node("Backgrounds/Background 2 2").hide()
	get_node("Backgrounds/Background 2 3").hide()
	get_node("Backgrounds/Background 2 4").hide()
	get_node("Level Section 2").hide()
	get_node("Section 3 Barrier").hide()
	get_node("Terminal 2").hide()
	get_node("Terminal 3").hide()
	get_node("Backgrounds/Background 3 1").hide()
	get_node("Level Section 4").hide()
	get_node("Exit Door").hide()
	
	scene_timer = get_node("Scene Timer")
	fire_rate_timer = get_node("Fire Rate Timer")
	toggle_shooting_timer = get_node("Toggle Shooting Timer")
	terminal_1_timer = get_node("Terminal 1 Timer")
	terminal_2_timer = get_node("Terminal 2 Timer")
	terminal_3_timer = get_node("Terminal 3 Timer")
	
	scene_timer.connect("timeout", self, "_on_scene_timer_timeout")
	fire_rate_timer.connect("timeout", self, "_on_fire_rate_timer_timeout")
	toggle_shooting_timer.connect("timeout", self, "_on_toggle_shooting_timer_timeout")
	terminal_1_timer.connect("timeout", self, "_on_terminal_1_timer_timeout")
	terminal_2_timer.connect("timeout", self, "_on_terminal_2_timer_timeout")
	terminal_3_timer.connect("timeout", self, "_on_terminal_3_timer_timeout")
	
	toggle_shooting_timer.set_wait_time(4.0)
	fire_rate_timer.set_wait_time(0.5)
	fire_rate_timer.start()
	scene_timer.set_wait_time(2.0)
	scene_timer.start()
	terminal_1_timer.set_wait_time(5.0)
	terminal_2_timer.set_wait_time(5.0)
	terminal_3_timer.set_wait_time(5.0)
	
	player.player_lives = 5
	player.player_can_move = false

func _process(delta):
	if not get_node("Dramatic Music").is_playing():
		get_node("Dramatic Music").play()
	if player.player_is_dead:
		player.player_lives -= 1
		player.position = player_starting_pos
		player_touching_terminal_1 = false
		player_touching_terminal_2 = false
		player_touching_terminal_3 = false
		if player.player_lives == 4:
			get_node("Player/Camera2D/Heart 5").hide()
		if player.player_lives == 3:
			get_node("Player/Camera2D/Heart 4").hide()
		if player.player_lives == 2:
			get_node("Player/Camera2D/Heart 3").hide()
		if player.player_lives == 1:
			get_node("Player/Camera2D/Heart 2").hide()
		if player.player_lives == 0:
			get_tree().change_scene("res://Assets/Scenes/GameOver.tscn")
		player.player_is_dead = false
	if not player_touching_terminal_1:
		terminal_1_timer.stop()
	if not player_touching_terminal_2:
		terminal_2_timer.stop()
	if not player_touching_terminal_3:
		terminal_3_timer.stop()

func _input(event):
	if event is InputEventMouseMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	elif event is InputEventJoypadButton or event is InputEventJoypadMotion:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	if event.is_action_pressed("use"):
		if player_touching_terminal_1 and terminal_1_activated == false:
			terminal_1_timer.start()
			get_node("Player/Terminal Progress Bar").show()
			get_node("Player/Terminal Progress Bar").play("5_sec")
		if player_touching_terminal_2 and terminal_2_activated == false:
			terminal_2_timer.start()
			get_node("Player/Terminal Progress Bar").show()
			get_node("Player/Terminal Progress Bar").play("5_sec")
		if player_touching_terminal_3 and terminal_3_activated == false:
			terminal_3_timer.start()
			get_node("Player/Terminal Progress Bar").show()
			get_node("Player/Terminal Progress Bar").play("5_sec")
		if player_touching_door:
			get_tree().change_scene("res://Assets/Scenes/Cutscene3.tscn")
			var save_data = {
				music_pos = get_node("Dramatic Music").get_playback_position(),
				current_level = 3,
				chosen_player = player.chosen_player,
				player_lives = player.player_lives
				}
			var file = File.new()
			file.open("res://savegame.save", File.WRITE)
			file.store_line(to_json(save_data))
			file.close()
	elif event.is_action_released("use"):
			terminal_1_timer.stop()
			terminal_2_timer.stop()
			terminal_3_timer.stop()
			get_node("Player/Terminal Progress Bar").hide()
			get_node("Player/Terminal Progress Bar").play("idle")
	if event.is_action_pressed("ui_pause"):
		if not $Mead/Camera2D.is_current():
			pause_menu_node = pause_menu.instance()
			add_child(pause_menu_node)
			$"Pause Menu".position = $Player.position - Vector2(512, 300)
			get_tree().paused = true

func _on_scene_timer_timeout():
	if scene_num == 0:
		get_node("Mead/Speech 1").show()
		get_node("Mead/Camera2D").make_current()
		scene_timer.set_wait_time(4.0)
		scene_timer.start()
		scene_num += 1
	elif scene_num == 1:
		get_node("Player/Camera2D").make_current()
		get_node("Player/Speech 1").show()
		get_node("Mead/Speech 1").hide()
		scene_timer.set_wait_time(2.0)
		scene_timer.start()
		scene_num += 1
	elif scene_num == 2:
		get_node("Mead/Camera2D").make_current()
		get_node("Mead/Speech 2").show()
		get_node("Player/Speech 1").hide()
		scene_num += 1
	elif scene_num == 3:
		get_node("Player/Camera2D").make_current()
		get_node("Mead/Speech 2").hide()
		player.player_can_move = true
		toggle_shooting_timer.start()
		mead_can_shoot = true
		scene_num += 1
		scene_timer.stop()
	elif scene_num == 4:
		get_node("Player/Speech 2").hide()
		player.player_can_move = true
		scene_num += 1
		scene_timer.stop()

func _on_fire_rate_timer_timeout():
	if mead_can_shoot:
		get_node("Mead").shoot()

func _on_toggle_shooting_timer_timeout():
	if mead_can_shoot == true:
		mead_can_shoot = false
	elif mead_can_shoot == false:
		mead_can_shoot = true

func _on_terminal_1_timer_timeout():
	terminal_1_activated = true
	terminal_1_timer.stop()
	get_node("Player/Terminal Progress Bar").hide()
	get_node("Player/Terminal Progress Bar").play("idle")
	get_node("Terminal 1/AnimatedSprite").play("activated")
	get_node("Section 2 Barrier").queue_free()
	get_node("Backgrounds/Background 2 1").show()
	get_node("Backgrounds/Background 2 2").show()
	get_node("Level Section 2").show()
	get_node("Section 3 Barrier").show()
	get_node("Terminal 2").show()

func _on_terminal_2_timer_timeout():
	terminal_2_activated = true
	terminal_2_timer.stop()
	get_node("Player/Terminal Progress Bar").hide()
	get_node("Player/Terminal Progress Bar").play("idle")
	get_node("Terminal 2/AnimatedSprite").play("activated")
	get_node("Section 3 Barrier").queue_free()
	get_node("Section 4 Barrier").show()
	get_node("Level Section 3").show()
	get_node("Backgrounds/Background 2 3").show()
	get_node("Backgrounds/Background 2 4").show()
	get_node("Terminal 3").show()

func _on_terminal_3_timer_timeout():
	terminal_3_activated = true
	terminal_3_timer.stop()
	get_node("Player/Terminal Progress Bar").hide()
	get_node("Player/Terminal Progress Bar").play("idle")
	get_node("Terminal 3/AnimatedSprite").play("activated")
	get_node("Backgrounds/Background 3 1").show()
	get_node("Section 4 Barrier").queue_free()
	get_node("Level Section 4").show()
	get_node("Exit Door").show()


func _on_Terminal_1_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_terminal_1 = true
		if shown_player_speech_2 == false:
			player.player_can_move = false
			get_node("Player/Speech 2").show()
			scene_timer.set_wait_time(4.0)
			scene_timer.start()
			shown_player_speech_2 = true


func _on_Terminal_1_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		$"Player/Terminal Progress Bar".hide()
		player_touching_terminal_1 = false


func _on_Terminal_2_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_terminal_2 = true


func _on_Terminal_2_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		$"Player/Terminal Progress Bar".hide()
		player_touching_terminal_2 = false


func _on_Terminal_3_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_terminal_3 = true


func _on_Terminal_3_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		$"Player/Terminal Progress Bar".hide()
		player_touching_terminal_3 = false


func _on_Door_Activation_Area_body_entered(body):
	if body.get_name() == "Player":
		player_touching_door = true


func _on_Door_Activation_Area_body_exited(body):
	if body.get_name() == "Player":
		player_touching_door = false
